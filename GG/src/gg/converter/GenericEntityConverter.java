package gg.converter;

import gg.constants.Constants;
import gg.entity.GenericEntity;
import gg.managed.bean.imp.GenericManagedBeanImp;
import gg.services.ReadService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "entityConverter")
public class GenericEntityConverter extends GenericManagedBeanImp implements
		Converter {
	ReadService readService;

	public GenericEntityConverter() {
		readService = super.findBean(ReadService.class);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		try {
			String[] classId = value.split(Constants.DEFAULT_SEPARATOR);
			Class class_ = Class.forName(classId[0]);
			Integer id = Integer.parseInt(classId[1]);
			return readService.readById(id, class_);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return value.getClass().getName() + Constants.DEFAULT_SEPARATOR
				+ ((GenericEntity) value).getId();
	}
}