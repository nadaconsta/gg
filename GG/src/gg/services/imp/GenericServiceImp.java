package gg.services.imp;

import gg.managed.bean.imp.GenericManagedBeanImp;
import gg.services.GenericService;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@ApplicationScoped
@ManagedBean(eager = true)
// This is the class used to implement one method to all services
public class GenericServiceImp extends GenericManagedBeanImp implements
		GenericService {

	@Override
	public void init() {
		throw new NotImplementedException();
	}

}
