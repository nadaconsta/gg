package gg.services.imp;

import gg.dao.DayDao;
import gg.dao.GameDao;
import gg.dao.GenericDao;
import gg.dao.ProficiencyDao;
import gg.dao.UserDao;
import gg.entity.Day;
import gg.entity.Game;
import gg.entity.GenericEntity;
import gg.entity.Proficiency;
import gg.entity.User;
import gg.services.ReadService;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(eager = true)
public class ReadServiceImp extends GenericServiceImp implements ReadService {
	GenericDao genericDao;
	UserDao userDao;
	GameDao gameDao;
	ProficiencyDao proficiencyDao;
	DayDao dayDao;

	@Override
	@PostConstruct
	public void init() {
		userDao = super.findBean(UserDao.class);
		gameDao = super.findBean(GameDao.class);
		proficiencyDao = super.findBean(ProficiencyDao.class);
		dayDao = super.findBean(DayDao.class);
	}

	@Override
	public User readUserByEmailPassword(User user) {
		return userDao.findUserByEmailPassword(user);
	}

	@Override
	public <T> GenericEntity<T> readById(Integer id, Class<T> class_) {
		genericDao = super.findBean(GenericDao.class);
		return genericDao.findById(id, class_);
	}

	@Override
	public List<Game> readAllGames() {
		return gameDao.findAllGames();
	}

	@Override
	public List<Day> readAllDays() {
		return dayDao.findAllDays();
	}

	@Override
	public List<Proficiency> readProficiencyByType(String type) {
		return proficiencyDao.findProficiencyByType(type);
	}

}
