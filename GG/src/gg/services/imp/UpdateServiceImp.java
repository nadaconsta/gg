package gg.services.imp;

import gg.dao.ScheduleDao;
import gg.dao.ScheduleTimeDao;
import gg.dao.UserDao;
import gg.dao.UserGameDao;
import gg.entity.Schedule;
import gg.entity.ScheduleTime;
import gg.entity.User;
import gg.entity.UserGame;
import gg.services.UpdateService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(eager = true)
public class UpdateServiceImp extends GenericServiceImp implements
		UpdateService {
	UserDao userDao;
	UserGameDao userGameDao;
	ScheduleDao scheduleDao;
	ScheduleTimeDao scheduleTimeDao;

	@Override
	@PostConstruct
	public void init() {
		userDao = super.findBean(UserDao.class);
		userGameDao = super.findBean(UserGameDao.class);
		scheduleDao = super.findBean(ScheduleDao.class);
		scheduleTimeDao = super.findBean(ScheduleTimeDao.class);

	}

	@Override
	public User updateUser(User user) {
		return userDao.updateUser(user);
	}

	@Override
	public UserGame updateUserGame(UserGame userGame) {
		User user = userGame.getUser();
		userGame = userGameDao.updateUserGame(userGame);
		user.removeUserGame(userGame);
		user.addUserGame(userGame);
		return userGame;
	}

	@Override
	public Schedule updateSchedule(Schedule schedule) {
		User user = schedule.getUser();
		UserGame userGame = schedule.getUserGame();
		schedule = scheduleDao.updateSchedule(schedule);
		if (user != null) {
			user.setSchedule(schedule);
		} else if (userGame != null) {
			userGame.setSchedule(schedule);
		}
		return schedule;
	}

	@Override
	public ScheduleTime updateScheduleTime(ScheduleTime scheduleTime) {
		Schedule schedule = scheduleTime.getSchedule();
		scheduleTime = scheduleTimeDao.updateScheduleTime(scheduleTime);
		schedule.removeScheduleTime(scheduleTime);
		schedule.addScheduleTime(scheduleTime);
		return scheduleTime;
	}

}
