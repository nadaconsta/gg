package gg.services;

import gg.entity.GroupPattern;
import gg.entity.Schedule;
import gg.entity.ScheduleTime;
import gg.entity.Server;
import gg.entity.User;
import gg.entity.UserGame;

public interface CreateService extends GenericService {
	User createUser(User user);

	UserGame createUserGame(UserGame userGame);

	Schedule createSchedule(Schedule schedule);

	ScheduleTime createScheduleTime(ScheduleTime scheduleTime);
	
	Server createServer(Server server);
	
	GroupPattern createGroupPattern(GroupPattern groupPattern);
}
