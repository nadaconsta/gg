package gg.services;

import gg.entity.Schedule;
import gg.entity.ScheduleTime;
import gg.entity.User;
import gg.entity.UserGame;

public interface UpdateService extends GenericService {
	User updateUser(User user);

	UserGame updateUserGame(UserGame userGame);

	Schedule updateSchedule(Schedule schedule);

	ScheduleTime updateScheduleTime(ScheduleTime scheduleTime);
}
