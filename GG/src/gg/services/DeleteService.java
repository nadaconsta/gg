package gg.services;

import gg.entity.Schedule;
import gg.entity.ScheduleTime;
import gg.entity.UserGame;

public interface DeleteService extends GenericService {

	void deleteUserGame(UserGame userGame);

	void deleteScheduleTime(ScheduleTime scheduleTime);

	void deleteSchedule(Schedule schedule);

}
