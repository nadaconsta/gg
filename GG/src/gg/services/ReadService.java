package gg.services;

import gg.entity.Day;
import gg.entity.Game;
import gg.entity.GenericEntity;
import gg.entity.Proficiency;
import gg.entity.User;

import java.util.List;

public interface ReadService extends GenericService {
	<T> GenericEntity<T> readById(Integer id, Class<T> class_);

	// Get the user using specific parameter of a non persisted entity
	User readUserByEmailPassword(User user);

	List<Game> readAllGames();

	List<Proficiency> readProficiencyByType(String type);

	List<Day> readAllDays();

}
