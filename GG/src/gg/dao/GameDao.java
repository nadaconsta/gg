package gg.dao;

import gg.entity.Game;

import java.util.List;

public interface GameDao extends GenericDao{
	public List<Game> findAllGames();
}
