package gg.dao;

import gg.entity.GroupPattern;

public interface GroupPatternDao extends GenericDao {
	Boolean deleteGroupPattern(GroupPattern groupPattern);

	GroupPattern createGroupPattern(GroupPattern groupPattern);

	GroupPattern updateGroupPattern(GroupPattern groupPattern);

}
