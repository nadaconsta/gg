package gg.dao;

import gg.entity.Day;

import java.util.List;

public interface DayDao extends GenericDao{
	public List<Day> findAllDays();
}
