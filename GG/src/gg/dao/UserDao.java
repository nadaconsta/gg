package gg.dao;

import gg.entity.User;

import javax.persistence.NoResultException;

public interface UserDao extends GenericDao {
	User createUser(User user);

	User updateUser(User user);

	User findUserByEmailPassword(User user) throws NoResultException;

}
