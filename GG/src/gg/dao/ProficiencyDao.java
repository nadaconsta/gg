package gg.dao;

import gg.entity.Proficiency;

import java.util.List;

public interface ProficiencyDao extends GenericDao{
	List<Proficiency> findProficiencyByType(String type);
}
