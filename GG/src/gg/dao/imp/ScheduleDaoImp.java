package gg.dao.imp;

import gg.dao.ScheduleDao;
import gg.entity.Schedule;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(eager = true)
public class ScheduleDaoImp extends GenericDaoImp implements ScheduleDao {

	@Override
	public Boolean deleteSchedule(Schedule schedule) {
		return delete(schedule);
	}

	@Override
	public Schedule createSchedule(Schedule schedule) {
		return (Schedule) saveOrUpdate(schedule);
	}

	@Override
	public Schedule updateSchedule(Schedule schedule) {
		return (Schedule) saveOrUpdate(schedule);
	}

}