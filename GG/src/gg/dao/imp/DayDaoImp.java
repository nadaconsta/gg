package gg.dao.imp;

import gg.dao.DayDao;
import gg.entity.Day;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;

@ApplicationScoped
@ManagedBean(eager = true)
public class DayDaoImp extends GenericDaoImp implements DayDao {

	@Override
	public List<Day> findAllDays() {
		TypedQuery<Day> query = createQuery("Day.findAll", Day.class);
		return query.getResultList();
	}

}
