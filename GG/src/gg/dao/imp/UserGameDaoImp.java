package gg.dao.imp;

import gg.dao.UserGameDao;
import gg.entity.UserGame;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(eager = true)
public class UserGameDaoImp extends GenericDaoImp implements UserGameDao {

	@Override
	public Boolean deleteUserGame(UserGame userGame) {
		return delete(userGame);
	}

	@Override
	public UserGame createUserGame(UserGame userGame) {
		return (UserGame) saveOrUpdate(userGame);
	}

	@Override
	public UserGame updateUserGame(UserGame userGame) {
		return (UserGame) saveOrUpdate(userGame);
	}

}