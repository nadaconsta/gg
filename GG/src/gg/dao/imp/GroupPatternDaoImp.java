package gg.dao.imp;

import gg.dao.GroupPatternDao;
import gg.entity.GroupPattern;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(eager = true)
public class GroupPatternDaoImp extends GenericDaoImp implements GroupPatternDao {

	@Override
	public Boolean deleteGroupPattern(GroupPattern groupPattern) {
		return delete(groupPattern);
	}

	@Override
	public GroupPattern createGroupPattern(GroupPattern groupPattern) {
		return (GroupPattern) saveOrUpdate(groupPattern);
	}

	@Override
	public GroupPattern updateGroupPattern(GroupPattern groupPattern) {
		return (GroupPattern) saveOrUpdate(groupPattern);
	}

}