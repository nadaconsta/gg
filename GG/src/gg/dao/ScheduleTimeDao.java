package gg.dao;

import gg.entity.ScheduleTime;

public interface ScheduleTimeDao extends GenericDao {
	Boolean deleteScheduleTime(ScheduleTime scheduleTime);

	ScheduleTime createScheduleTime(ScheduleTime scheduleTime);

	ScheduleTime updateScheduleTime(ScheduleTime scheduleTime);
}
