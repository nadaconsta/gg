package gg.controllers;

import gg.entity.UserGame;

import java.util.List;

public interface UserGameCrudController extends GenericController {

	String actionSaveUserGame();

	String redirectHome();

	String redirectAccountUserGameCrud();

	List<UserGame> getSelectUserGames();

	void setSelectUserGames(List<UserGame> lsSelectUserGames);

}
