package gg.controllers;

import gg.entity.User;

public interface UserRegisterController extends GenericController {

	String actionSaveUser();

	String redirectIndex();

	User getUser();

	void setUser(User user);

}
