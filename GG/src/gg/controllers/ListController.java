package gg.controllers;

import gg.entity.Day;
import gg.entity.Game;
import gg.entity.Proficiency;

import java.util.List;


public interface ListController extends GenericController {

	List<Proficiency> getUserProficiency();

	List<Proficiency> getUserGameProficiency();

	List<Game> getAllGames();

	List<Day> getScheduleTimeDay();


}
