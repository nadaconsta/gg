package gg.controllers;

import gg.entity.User;

public interface HomeController extends GenericController {

	String actionLogout();

	String redirectUserGameCrud();

	String redirectScheduleCrud();

	String redirectServerCrud();

	String redirectGroupPatternCrud();
	
	User getUser();

	void setUser(User user);

}
