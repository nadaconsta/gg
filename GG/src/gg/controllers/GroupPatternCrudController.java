package gg.controllers;

import gg.entity.GroupPattern;

public interface GroupPatternCrudController extends GenericController {
	
	String actionSaveGroupPattern();

	String redirectIndex();

	GroupPattern getGroupPattern();

	void setGroupPattern(GroupPattern groupPattern);

}