package gg.controllers.imp;

import gg.controllers.GenericController;
import gg.entity.GenericEntity;
import gg.exceptions.ErrorException;
import gg.exceptions.InfoException;
import gg.exceptions.WarnException;
import gg.managed.bean.imp.GenericManagedBeanImp;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@SessionScoped
@ManagedBean(eager = true)
public class GenericControllerImp extends GenericManagedBeanImp implements
		GenericController {
	GenericEntity<?> entity;

	@Override
	public void init() {
		throw new NotImplementedException();
	}

	@Override
	public void sendMessage(Exception e) {
		if (InfoException.class.equals(e.getClass())) {
			InfoException me = (InfoException) e;
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, me.getCode()
							.toString(), me.getMessage()));
		} else if (WarnException.class.equals(e.getClass())) {
			WarnException me = (WarnException) e;
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, me.getCode()
							.toString(), me.getMessage()));
		} else if (ErrorException.class.equals(e.getClass())) {
			ErrorException ee = (ErrorException) e;
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, ee.getCode()
							.toString(), ee.getMessage()));
		} else {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "999999",
							"Fatal Error."));
		}
	}
	
	@Override
	public GenericEntity<?> getEntity(){
		return this.entity;
	}
	
	@Override
	public void setEntity(GenericEntity<?> genericEntity){
		this.entity = genericEntity;
	}
}
