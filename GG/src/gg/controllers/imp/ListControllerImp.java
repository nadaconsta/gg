package gg.controllers.imp;

import gg.constants.Constants;
import gg.controllers.ListController;
import gg.entity.Day;
import gg.entity.Game;
import gg.entity.Proficiency;
import gg.services.ReadService;

import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ViewScoped
@ManagedBean(eager = true)
public class ListControllerImp extends GenericControllerImp implements
		ListController {

	private ReadService readService;
	private ResourceBundle msgsBundle;

	@Override
	@PostConstruct
	public void init() {
		readService = super.findBean(ReadService.class);
		msgsBundle = super.getMsgsBundle();
	}

	@Override
	public List<Proficiency> getUserGameProficiency() {
		List<Proficiency> lsProficiency = readService
				.readProficiencyByType(Constants.USERGAME_PROFICIENCY_TYPE);
		for (Proficiency proficiency : lsProficiency) {
			String description = Constants.PROFICIENCY_DESC
					+ Constants.USERGAME_PROFICIENCY_TYPE
					+ proficiency.getDesc();
			proficiency.setDescription(msgsBundle.getString(description));
		}
		return lsProficiency;
	}

	@Override
	public List<Proficiency> getUserProficiency() {
		List<Proficiency> lsProficiency = readService
				.readProficiencyByType(Constants.USER_PROFICIENCY_TYPE);
		for (Proficiency proficiency : lsProficiency) {
			String description = Constants.PROFICIENCY_DESC
					+ Constants.USER_PROFICIENCY_TYPE + proficiency.getDesc();
			proficiency.setDescription(msgsBundle.getString(description));
		}
		return lsProficiency;
	}

	@Override
	public List<Day> getScheduleTimeDay() {
		List<Day> lsDay = readService.readAllDays();
		for (Day day : lsDay) {
			String description = Constants.DAY_DESC + day.getDesc();
			day.setDescription(msgsBundle.getString(description));
		}
		return lsDay;
	}

	@Override
	public List<Game> getAllGames() {
		return readService.readAllGames();
	}

}
