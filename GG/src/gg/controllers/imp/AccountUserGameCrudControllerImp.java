package gg.controllers.imp;

import gg.constants.Constants;
import gg.controllers.AccountUserGameCrudController;
import gg.controllers.GenericController;
import gg.controllers.HomeController;
import gg.entity.AccountUserGame;
import gg.entity.Server;
import gg.entity.UserGame;
import gg.services.CreateService;

import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ViewScoped
@ManagedBean(eager = true)
public class AccountUserGameCrudControllerImp extends GenericControllerImp
		implements AccountUserGameCrudController {

	// Entities
	private AccountUserGame accountUserGame;

	// Services
	private CreateService createService;
	private GenericController genericController;
	private ResourceBundle msgsBundle;

	@Override
	@PostConstruct
	public void init() {
		this.accountUserGame = new AccountUserGame();
		this.genericController = super.findBean(GenericController.class);
		this.accountUserGame.setUserGame((UserGame) this.genericController
				.getEntity());
		this.createService = super.findBean(CreateService.class);
		msgsBundle = super.getMsgsBundle();
	}

	@Override
	public String actionSaveAccountUserGame() {
		try {
			// this.accountUserGame = createService
			// .createAccountUserGame(this.accountUserGame);
			return getBeanName(IndexControllerImp.class);
		} catch (Exception e) {
			sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public String redirectHome() {
		return getBeanName(HomeController.class);
	}

	// Getters and Setters
	@Override
	public List<Server> getAccountUserGameServer() {
		List<Server> lsServer = this.accountUserGame.getUserGame().getGame()
				.getServers();
		for (Server server : lsServer) {
			String description = Constants.SERVER_DESC
					+ server.getGame().getAcronym() + server.getDesc();
			server.setDescription(msgsBundle.getString(description));
		}
		return lsServer;
	}

	@Override
	public AccountUserGame getAccountUserGame() {
		return this.accountUserGame;
	}

	@Override
	public void setAccountUserGame(AccountUserGame accountUserGame) {
		this.accountUserGame = accountUserGame;
	}

}