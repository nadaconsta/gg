package gg.controllers.imp;

import gg.controllers.ServerCrudController;
import gg.entity.Server;
import gg.services.CreateService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ViewScoped
@ManagedBean(eager = true)
public class ServerCrudControllerImp extends GenericControllerImp implements
		ServerCrudController {

	// Entities
	private Server server;

	// Services
	private CreateService createService;

	@Override
	@PostConstruct
	public void init() {
		this.server = new Server();
		this.createService = super.findBean(CreateService.class);
	}

	@Override
	public String actionSaveServer() {
		try {
			this.server = createService.createServer(this.server);
			return getBeanName(IndexControllerImp.class);
		} catch (Exception e) {
			sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public String redirectIndex() {
		return getBeanName(IndexControllerImp.class);
	}

	// Getters and Setters
	@Override
	public Server getServer() {
		return this.server;
	}

	@Override
	public void setServer(Server server) {
		this.server = server;
	}

}