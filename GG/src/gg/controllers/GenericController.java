package gg.controllers;

import gg.entity.GenericEntity;
import gg.managed.bean.GenericManagedBean;

public interface GenericController extends GenericManagedBean {

	public void init();

	// This method should treat all gg specific exceptions to a proper message.
	public void sendMessage(Exception e);

	void setEntity(GenericEntity<?> genericEntity);

	GenericEntity<?> getEntity();

}
