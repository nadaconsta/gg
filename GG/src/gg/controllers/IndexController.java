package gg.controllers;

import gg.entity.User;

public interface IndexController extends GenericController {

	public String actionLogin();

	public String redirectUserRegistration();

	public User getUser();

	public void setUser(User user);

}
