package gg.controllers;

import java.util.List;

import gg.entity.AccountUserGame;
import gg.entity.Server;

public interface AccountUserGameCrudController extends GenericController {

	String actionSaveAccountUserGame();

	String redirectHome();

	AccountUserGame getAccountUserGame();
	
	List<Server> getAccountUserGameServer();

	void setAccountUserGame(AccountUserGame accountUserGame);

}
