package gg.controllers;

import gg.entity.Schedule;
import gg.entity.ScheduleTime;

import java.util.List;

public interface ScheduleCrudController extends GenericController {

	String actionSaveSchedule();

	String actionAddScheduleTime();

	String actionDelScheduleTime();

	String redirectHome();

	List<Schedule> getSelectSchedules();

	void setSelectSchedules(List<Schedule> lsSelectSchedule);

	Schedule getSchedule();

	void setSchedule(Schedule schedule);

	ScheduleTime getScheduleTime();

	void setScheduleTime(ScheduleTime scheduleTime);

}
